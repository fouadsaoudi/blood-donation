package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.Switch;
import android.widget.Toast;

import com.example.myapplication.util.SavedPreferences;
import com.example.myapplication.util.sprint.APIEndPoint;
import com.example.myapplication.util.sprint.Method;
import com.example.myapplication.util.sprint.Sprint;
import com.example.myapplication.util.sprint.SprintCallback;
import com.google.firebase.iid.FirebaseInstanceId;

import org.json.JSONObject;

import java.util.HashMap;

public class SettingActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        // initiate a Switch
        Switch simpleSwitch = findViewById(R.id.sw_notification);
        simpleSwitch.setChecked(SavedPreferences.getRINGTONE(this));

        simpleSwitch.setOnCheckedChangeListener((buttonView, isChecked) -> {
            SavedPreferences.setRINGTONE(SettingActivity.this, isChecked);
            FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(command -> {
                HashMap<String, String> args = new HashMap<>();
                args.put("user_id", SavedPreferences.getUserId(this));
                args.put("device_token", command.getToken());
                args.put("silent", isChecked ? "1" : "0");
                Sprint.instance(Method.POST)
                        .request(APIEndPoint.NOTIFICATION, args)
                        .execute(new SprintCallback() {
                            @Override
                            public void onSuccess(JSONObject data) {
                                boolean silent = data.optString("silent").equals("1");
                                SavedPreferences.setRINGTONE(SettingActivity.this, silent);
                            }

                            @Override
                            public void onFailure(JSONObject data, int statusCode) {
                                boolean silent = data.optString("silent").equals("1");
                                SavedPreferences.setRINGTONE(SettingActivity.this, silent);
                            }

                            @Override
                            public void onException(Exception e) {
                                e.printStackTrace();
                            }
                        });
            });
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
