package com.example.myapplication.util.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.myapplication.R;
import com.example.myapplication.util.model.Doner;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class DonerAdapter extends RecyclerView.Adapter<DonerAdapter.MyViewHolder> {
    private ArrayList<Doner> arrayList;
    private Context context;

    public DonerAdapter(ArrayList<Doner> arrayList, Context context) {
        this.arrayList = arrayList;
        this.context = context;
    }

    @NonNull
    @Override
    public DonerAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new DonerAdapter.MyViewHolder(LayoutInflater.from(context).inflate(R.layout.layout_guest, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull DonerAdapter.MyViewHolder holder, int position) {
        Doner doner = arrayList.get(position);
        holder.name.setText(doner.getName());
        holder.phoneNumber.setText(doner.getPhoneNumber());
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView name, phoneNumber;
        MyViewHolder(@NonNull View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.tv_donor_name);
            phoneNumber = itemView.findViewById(R.id.tv_donor_number);
        }
    }
}
