package com.example.myapplication.util.model;

import java.io.Serializable;

public class Patient implements Serializable {
    private String id, fname, lname, contactNumber, dob, caseDescription, gender, bloodType, usersId;

    public Patient(String id, String fname, String lname, String contactNumber, String dob, String caseDescription, String gender, String bloodType, String usersId) {
        this.id = id;
        this.fname = fname;
        this.lname = lname;
        this.contactNumber = contactNumber;
        this.dob = dob;
        this.caseDescription = caseDescription;
        this.gender = gender;
        this.bloodType = bloodType;
        this.usersId = usersId;
    }

    public Patient(String fname, String lname, String contactNumber, String caseDescription) {
        this.fname = fname;
        this.lname = lname;
        this.contactNumber = contactNumber;
        this.caseDescription = caseDescription;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public String getLname() {
        return lname;
    }

    public void setLname(String lname) {
        this.lname = lname;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getCaseDescription() {
        return caseDescription;
    }

    public void setCaseDescription(String caseDescription) {
        this.caseDescription = caseDescription;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getBloodType() {
        return bloodType;
    }

    public void setBloodType(String bloodType) {
        this.bloodType = bloodType;
    }

    public String getUsersId() {
        return usersId;
    }

    public void setUsersId(String usersId) {
        this.usersId = usersId;
    }

    public String getFullName(){
        return getFname() + " " + getLname();
    }
}
