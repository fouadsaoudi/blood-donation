package com.example.myapplication.util.listeners;

import com.example.myapplication.util.model.Request;

public interface RequestCallback {
    void onClick(Request request);
}
