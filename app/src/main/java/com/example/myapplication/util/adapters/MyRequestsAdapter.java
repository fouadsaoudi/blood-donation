package com.example.myapplication.util.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.myapplication.R;
import com.example.myapplication.util.listeners.MyRequestsCallback;
import com.example.myapplication.util.model.MyRequests;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class MyRequestsAdapter extends RecyclerView.Adapter<MyRequestsAdapter.MyViewHolder> {
    private ArrayList <MyRequests> arrayList;
    private Context context;
    private MyRequestsCallback myRequestsCallback;

    public MyRequestsAdapter(ArrayList<MyRequests> arrayList, Context context, MyRequestsCallback myRequestsCallback) {
        this.arrayList = arrayList;
        this.context = context;
        this.myRequestsCallback = myRequestsCallback;
    }

    @NonNull
    @Override
    public MyRequestsAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyRequestsAdapter.MyViewHolder(LayoutInflater.from(context).inflate(R.layout.layout_my_request, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyRequestsAdapter.MyViewHolder holder, int position) {
        MyRequests myRequests = arrayList.get(position);
        holder.patientName.setText(myRequests.getFullName());
        holder.requestDate.setText(myRequests.getRequestDate());
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView patientName, requestDate;
        MyViewHolder(@NonNull View itemView) {
            super(itemView);
            patientName = itemView.findViewById(R.id.tv_patient_name);
            requestDate = itemView.findViewById(R.id.tv_request_date);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            myRequestsCallback.onClick(arrayList.get(getLayoutPosition()));
        }
    }
}
