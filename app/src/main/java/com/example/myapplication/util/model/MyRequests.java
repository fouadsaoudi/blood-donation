package com.example.myapplication.util.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;
import java.util.ArrayList;

public class MyRequests implements Serializable, Parcelable {
    private String id, requestDate, fullName;
    private int plateletsNumber, unitsNumber, remainingPlatelets, remainingUnits;
    private boolean caseFinished;
    private ArrayList <Donations> donationsArrayList;

    public MyRequests(String id, String requestDate, String fullName,
                      int plateletsNumber, int unitsNumber, int remainingPlatelets,
                      int remainingUnits, boolean caseFinished,
                      ArrayList<Donations> donationsArrayList) {
        this.id = id;
        this.requestDate = requestDate;
        this.fullName = fullName;
        this.plateletsNumber = plateletsNumber;
        this.unitsNumber = unitsNumber;
        this.remainingPlatelets = remainingPlatelets;
        this.remainingUnits = remainingUnits;
        this.caseFinished = caseFinished;
        this.donationsArrayList = donationsArrayList;
    }

    protected MyRequests(Parcel in) {
        id = in.readString();
        requestDate = in.readString();
        fullName = in.readString();
        plateletsNumber = in.readInt();
        unitsNumber = in.readInt();
        remainingPlatelets = in.readInt();
        remainingUnits = in.readInt();
        caseFinished = in.readByte() != 0;
        donationsArrayList = in.createTypedArrayList(Donations.CREATOR);
    }

    public static final Creator<MyRequests> CREATOR = new Creator<MyRequests>() {
        @Override
        public MyRequests createFromParcel(Parcel in) {
            return new MyRequests(in);
        }

        @Override
        public MyRequests[] newArray(int size) {
            return new MyRequests[size];
        }
    };

    public void setDonationsArrayList(ArrayList<Donations> donationsArrayList) {
        this.donationsArrayList = donationsArrayList;
    }

    public String getId() {
        return id;
    }

    public String getRequestDate() {
        return requestDate;
    }

    public String getFullName() {
        return fullName;
    }

    public int getPlateletsNumber() {
        return plateletsNumber;
    }

    public int getUnitsNumber() {
        return unitsNumber;
    }

    public int getRemainingPlatelets() {
        return remainingPlatelets;
    }

    public int getRemainingUnits() {
        return remainingUnits;
    }

    public boolean isCaseFinished() {
        return caseFinished;
    }

    public ArrayList<Donations> getDonationsArrayList() {
        return donationsArrayList;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(requestDate);
        dest.writeString(fullName);
        dest.writeInt(plateletsNumber);
        dest.writeInt(unitsNumber);
        dest.writeInt(remainingPlatelets);
        dest.writeInt(remainingUnits);
        dest.writeByte((byte) (caseFinished ? 1 : 0));
        dest.writeTypedList(donationsArrayList);
    }
}
