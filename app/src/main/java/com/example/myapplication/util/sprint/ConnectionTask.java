package com.example.myapplication.util.sprint;

import android.os.AsyncTask;
import android.util.SparseArray;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

import com.example.myapplication.util.sprint.header.Header;
import com.example.myapplication.util.sprint.multipart.MultiPartFile;

public class ConnectionTask extends AsyncTask<Void, Integer, SparseArray<String>> {

    private String url;
    private HashSet<Header> headers;
    private HashMap<String, String> arguments;
    private ArrayList<MultiPartFile> files;
    private Method method;
    private SprintCallback callback;

    ConnectionTask(String url, HashMap<String, String> arguments, ArrayList<MultiPartFile> files, HashSet<Header> headers, Method method, SprintCallback callback) {
        this.url = url;
        this.callback = callback;
        this.arguments = arguments;
        this.method = method;
        this.headers = headers;
        this.files = files;
    }

    @Override
    protected SparseArray<String> doInBackground(Void... Void) {
        if (url == null) {
            callback.onException(new Exception("URL must not be empty"));
            return null;
        }
        SparseArray<String> result;
        try {
            SprintUtil util = new SprintUtil(url, headers, arguments, files, method);
            result = util.request();
            return result;
        } catch (Exception e) {
            callback.onException(e);
            return null;
        }
    }

    @Override
    protected void onPostExecute(SparseArray<String> result) {
        if (result != null) {
            if (result.size() == 0)
                callback.onException(new Exception());
            else if (result.size() == 1) {
                int key = result.keyAt(0);

                try {
                    if (key == HttpURLConnection.HTTP_OK)
                        callback.onSuccess(new JSONObject(result.get(key)));
                    else
                        callback.onFailure(new JSONObject(result.get(key)), key);
                } catch (JSONException e) {
                    callback.onException(e);
                }
            } else {
                callback.onException(new Exception());
            }
        }
    }
}
