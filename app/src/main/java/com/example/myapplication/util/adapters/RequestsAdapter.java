package com.example.myapplication.util.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.myapplication.R;
import com.example.myapplication.util.listeners.RequestCallback;
import com.example.myapplication.util.model.Request;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class RequestsAdapter extends RecyclerView.Adapter<RequestsAdapter.MyViewHolder> {

    private ArrayList<Request> arrayList;
    private Context context;
    private RequestCallback requestCallback;

    public RequestsAdapter(ArrayList<Request> arrayList, Context context, RequestCallback requestCallback) {
        this.arrayList = arrayList;
        this.context = context;
        this.requestCallback = requestCallback;
    }

    @NonNull
    @Override
    public RequestsAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(context).inflate(R.layout.layout_request, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RequestsAdapter.MyViewHolder holder, int position) {
        Request request = arrayList.get(position);
        holder.patientName.setText(request.getPatient().getFullName());
        holder.requestDate.setText(request.getRequestDate());
        holder.bloodType.setText(request.getPatient().getBloodType());
        holder.hospitalName.setText(request.getHospital().getName());

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        TextView patientName, requestDate, bloodType, hospitalName;
        MyViewHolder(@NonNull View itemView) {
            super(itemView);
            patientName = itemView.findViewById(R.id.tv_patient_name);
            requestDate = itemView.findViewById(R.id.tv_request_date);
            bloodType = itemView.findViewById(R.id.tv_blood_type);
            hospitalName = itemView.findViewById(R.id.tv_hospital_name);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            requestCallback.onClick(arrayList.get(getLayoutPosition()));
        }
    }
}
