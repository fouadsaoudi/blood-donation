package com.example.myapplication.util.model;

public class Doner {
    private String name, phoneNumber;

    public Doner(String name, String phoneNumber) {
        this.name = name;
        this.phoneNumber = phoneNumber;
    }

    public String getName() {
        return name;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }
}
