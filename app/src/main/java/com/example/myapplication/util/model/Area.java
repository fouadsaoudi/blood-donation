package com.example.myapplication.util.model;

import android.os.Parcel;
import android.os.Parcelable;

public class Area implements Parcelable {
    private String id, name;

    public Area(String id, String name) {
        this.id = id;
        this.name = name;
    }

    private Area(Parcel in) {
        id = in.readString();
        name = in.readString();
    }

    public static final Creator<Area> CREATOR = new Creator<Area>() {
        @Override
        public Area createFromParcel(Parcel in) {
            return new Area(in);
        }

        @Override
        public Area[] newArray(int size) {
            return new Area[size];
        }
    };

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(name);
    }
}
