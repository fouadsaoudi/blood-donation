package com.example.myapplication.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.Preference;
import android.preference.PreferenceManager;

public class SavedPreferences {
    public static void clear(Context context){
        PreferenceManager.getDefaultSharedPreferences(context).edit().clear().apply();
    }
    //user_id
    private final static String USER_ID = SavedPreferences.class.getName() + ".ID";
    public static void setUserId(final Context context, final String reg) {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(USER_ID, reg);
        editor.apply();
    }
    public static String getUserId(final Context context) {
        SharedPreferences language = PreferenceManager.getDefaultSharedPreferences(context);
        return language.getString(USER_ID, "");
    }
    //bloodType
    private final static String BLOOD_TYPE = SavedPreferences.class.getName() + ".ID";
    public static void setbloodType(final Context context, final String reg) {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(BLOOD_TYPE, reg);
        editor.apply();
    }
    public static String getbloodType(final Context context) {
        SharedPreferences language = PreferenceManager.getDefaultSharedPreferences(context);
        return language.getString(BLOOD_TYPE, "");
    }
    //Gender
    private final static String GENDER = SavedPreferences.class.getName() + ".ID";
    public static void setGenger(final Context context, final String reg) {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(GENDER, reg);
        editor.apply();
    }
    public static String getGender(final Context context) {
        SharedPreferences language = PreferenceManager.getDefaultSharedPreferences(context);
        return language.getString(GENDER, "");
    }
    //Email
    private final static String EMAIL = SavedPreferences.class.getName() + ".EMAIL";
    public static void setEmail(final Context context, final String reg) {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(EMAIL, reg);
        editor.apply();
    }
    public static String getEmail(final Context context) {
        SharedPreferences language = PreferenceManager.getDefaultSharedPreferences(context);
        return language.getString(EMAIL, "");
    }
    //FullName
    private final static String FULLNAME = SavedPreferences.class.getName() + ".FULLNAME";
    public static void setFullName(final Context context, final String reg) {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(FULLNAME, reg);
        editor.apply();
    }
    public static String getFullName(final Context context) {
        SharedPreferences language = PreferenceManager.getDefaultSharedPreferences(context);
        return language.getString(FULLNAME, "");
    }
    //UserName
    private final static String USERNAME = SavedPreferences.class.getName() + ".USERNAME";
    public static void setUserName (final Context context, final String reg) {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(USERNAME, reg);
        editor.apply();
    }
    public static String getUserName (final Context context) {
        SharedPreferences language = PreferenceManager.getDefaultSharedPreferences(context);
        return language.getString(USERNAME, "");
    }
    //Password
    private final static String PASSWORD = SavedPreferences.class.getName() + ".PASSWORD";
    public static void setPASSWORD (final Context context, final String reg) {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(PASSWORD, reg);
        editor.apply();
    }
    public static String getPASSWORD (final Context context) {
        SharedPreferences language = PreferenceManager.getDefaultSharedPreferences(context);
        return language.getString(PASSWORD, "");
    }
    //Number
    private final static String NUMBER = SavedPreferences.class.getName() + ".NUMBER";
    public static void setNumber (final Context context, final String reg) {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(NUMBER, reg);
        editor.apply();
    }
    public static String getNumber (final Context context) {
        SharedPreferences language = PreferenceManager.getDefaultSharedPreferences(context);
        return language.getString(NUMBER, "");
    }
    //Type
    private final static String TYPE = SavedPreferences.class.getName() + ".TYPE";
    public static void setType (final Context context, final String reg) {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(TYPE, reg);
        editor.apply();
    }
    public static String getType (final Context context) {
        SharedPreferences language = PreferenceManager.getDefaultSharedPreferences(context);
        return language.getString(TYPE, "");
    }
    //Date of Birth
    private final static String DOB = SavedPreferences.class.getName() + ".DOB";
    public static void setDOB (final Context context, final String reg) {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(DOB, reg);
        editor.apply();
    }
    public static String getDOB (final Context context) {
        SharedPreferences language = PreferenceManager.getDefaultSharedPreferences(context);
        return language.getString(DOB, "");
    }
    //Area
    private final static String AREA = SavedPreferences.class.getName() + ".AREA";
    public static void setArea (final Context context, final String reg) {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(AREA, reg);
        editor.apply();
    }
    public static String getArea (final Context context) {
        SharedPreferences language = PreferenceManager.getDefaultSharedPreferences(context);
        return language.getString(AREA, "");
    }
    //Address
    private final static String ADDRESS = SavedPreferences.class.getName() + ".ADDRESS";
    public static void setAddress (final Context context, final String reg) {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(ADDRESS, reg);
        editor.apply();
    }
    public static String getAddress (final Context context) {
        SharedPreferences language = PreferenceManager.getDefaultSharedPreferences(context);
        return language.getString(ADDRESS, "");
    }
    //checked
    private final static String CHECKED = SavedPreferences.class.getName() + ".CHECKED";
    public static void setCHECKED (final Context context, boolean reg) {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putBoolean(CHECKED, reg);
        editor.apply();
    }
    public static boolean getCHECKED (final Context context) {
        SharedPreferences language = PreferenceManager.getDefaultSharedPreferences(context);
        return language.getBoolean(CHECKED, false);
    }
    //RingToneChecked
    private final static String RINGTONE = SavedPreferences.class.getName() + ".RINGTONE";
    public static void setRINGTONE (final Context context, boolean reg) {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putBoolean(RINGTONE, reg);
        editor.apply();
    }
    public static boolean getRINGTONE (Context context) {
        SharedPreferences language = PreferenceManager.getDefaultSharedPreferences(context);
        return language.getBoolean(RINGTONE, false);
    }
}
