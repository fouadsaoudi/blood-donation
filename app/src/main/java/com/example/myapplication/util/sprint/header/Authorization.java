package com.example.myapplication.util.sprint.header;

public enum Authorization implements Header {
    BEARER("Bearer ");

    private String value;

    Authorization(String value) {
        this.value = value;
    }

    @Override
    public String key() {
        return "Authorization";
    }

    @Override
    public String value() {
        return value;
    }

    @Override
    public void value(String value) {
        this.value += value;
    }
}
