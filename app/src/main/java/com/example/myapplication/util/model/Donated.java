package com.example.myapplication.util.model;

public class Donated {
    private int donatedPlatelets, donatedUnits;
    private String dateOfDonation, phoneNumber, Name;

    public Donated(int donatedPlatelets, int donatedUnits, String dateOfDonation, String phoneNumber, String name) {
        this.donatedPlatelets = donatedPlatelets;
        this.donatedUnits = donatedUnits;
        this.dateOfDonation = dateOfDonation;
        this.phoneNumber = phoneNumber;
        Name = name;
    }

    public int getDonatedPlatelets() {
        return donatedPlatelets;
    }

    public int getDonatedUnits() {
        return donatedUnits;
    }

    public String getDateOfDonation() {
        return dateOfDonation;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String getName() {
        return Name;
    }
}
