package com.example.myapplication.util.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.myapplication.R;
import com.example.myapplication.util.model.Donated;
import com.example.myapplication.util.model.Donations;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class DonatedAdapter extends RecyclerView.Adapter<DonatedAdapter.MyViewHolder> {

    private Context context;
    private ArrayList<Donations> arrayList;

    public DonatedAdapter(Context context, ArrayList<Donations> arrayList) {
        this.context = context;
        this.arrayList = arrayList;
    }

    @NonNull
    @Override
    public DonatedAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(context).inflate(R.layout.layout_donated, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull DonatedAdapter.MyViewHolder holder, int position) {
        Donations donated = arrayList.get(position);
        holder.donnerName.setText(donated.getName());
        holder.donnerPhoneNumber.setText(donated.getPhoneNumber());
        holder.donationDate.setText(donated.getDate_of_donation());
        holder.donatedPlatelets.setText(donated.getDonated_platelets() + "");
        holder.donatedUnits.setText(donated.getDonated_units() + "");
    }

    @Override
    public int getItemCount() {
        return arrayList == null ? 0 : arrayList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView donatedPlatelets, donatedUnits, donnerName, donnerPhoneNumber, donationDate;
        MyViewHolder(@NonNull View itemView) {
            super(itemView);
            donatedPlatelets = itemView.findViewById(R.id.tv_remaining_platelets);
            donatedUnits = itemView.findViewById(R.id.tv_remaining_units);
            donnerName = itemView.findViewById(R.id.tv_donated_name);
            donnerPhoneNumber = itemView.findViewById(R.id.tv_phone_number);
            donationDate = itemView.findViewById(R.id.tv_donated_date);

        }
    }
}
