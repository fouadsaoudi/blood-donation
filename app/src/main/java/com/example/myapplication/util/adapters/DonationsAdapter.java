package com.example.myapplication.util.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.myapplication.R;
import com.example.myapplication.util.model.Donations;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class DonationsAdapter extends RecyclerView.Adapter<DonationsAdapter.MyViewHolder> {

    private ArrayList<Donations> arrayList;
    private Context context;

    public DonationsAdapter(ArrayList<Donations> arrayList, Context context) {
        this.arrayList = arrayList;
        this.context = context;
    }

    @NonNull
    @Override
    public DonationsAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(context).inflate(R.layout.layout_donations, parent, false ));
    }

    @Override
    public void onBindViewHolder(@NonNull DonationsAdapter.MyViewHolder holder, int position) {
        Donations donations = arrayList.get(position);
        holder.name.setText(donations.getPatient().getFullName());
        holder.phoneNumber.setText(donations.getPatient().getContactNumber());
        holder.caseDescription.setText(donations.getPatient().getCaseDescription());
        holder.date.setText(donations.getDate_of_donation());
        holder.plateletsDonated.setText(String.valueOf(donations.getDonated_platelets()));
        holder.unitsDonated.setText(String.valueOf(donations.getDonated_units()));
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView name, phoneNumber, caseDescription, date, plateletsDonated, unitsDonated;
        MyViewHolder(@NonNull View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.tv_patient_name);
            phoneNumber = itemView.findViewById(R.id.tv_patient_contact_number);
            caseDescription = itemView.findViewById(R.id.tv_case_description);
            date = itemView.findViewById(R.id.tv_donation_date);
            plateletsDonated = itemView.findViewById(R.id.tv_donated_platelets);
            unitsDonated = itemView.findViewById(R.id.tv_donated_units);
        }
    }
}
