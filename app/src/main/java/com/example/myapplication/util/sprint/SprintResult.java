package com.example.myapplication.util.sprint;

import android.os.AsyncTask;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

import com.example.myapplication.util.sprint.header.Header;
import com.example.myapplication.util.sprint.multipart.MultiPartFile;

public class SprintResult {

    private String url;
    private Method method;
    private ArrayList<MultiPartFile> files;
    private HashMap<String, String> arguments;
    private HashSet<Header> headers;
    private ConnectionTask task;

    SprintResult(String url, HashMap<String, String> arguments, ArrayList<MultiPartFile> files, HashSet<Header> headers, Method method) {
        this.url = url;
        this.method = method;
        this.arguments = arguments;
        this.files = files;
        this.headers = headers;
    }

    public void execute(SprintCallback callback) {

        task = new ConnectionTask(url, arguments, files, headers, method, callback);
        task.execute();
    }

    void cancel() {
        if (task.getStatus() != AsyncTask.Status.FINISHED)
            task.cancel(true);
    }

}