package com.example.myapplication.util.sprint;

import org.json.JSONObject;

public interface SprintCallback {
    void onSuccess(JSONObject data);

    void onFailure(JSONObject data, int statusCode);

    void onException(Exception e);
}
