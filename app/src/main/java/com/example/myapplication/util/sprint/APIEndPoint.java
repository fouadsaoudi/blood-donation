package com.example.myapplication.util.sprint;

public class APIEndPoint {
    private static final String URL = "http://172.105.80.216/";
    //GET
    private static final String GET = URL + "GET/";
    public static final String GET_ALL_REQUEST = GET + "get_all_requests.php";
    public static final String GET_AREAS = GET + "get_areas.php";
    public static final String GET_ALL_HOSPITALS = GET + "get_all_hospitals.php";

    //POST
    private static final String POST = URL + "POST/";
    public static final String REGISTER = POST + "register.php";
    public static final String LOGIN = POST + "login.php";
    public static final String DONATIONS = POST + "get_donations.php";
    public static final String MY_REQUESTS = POST + "get_donation_by_request.php";
    public static final String DONERS = POST + "search_doners.php";
    public static final String ADD_REQUEST = POST + "add_request.php";
    public static final String DONATE = POST + "donate.php";
    public static final String BLOOD_SEARCH = POST + "search_doners01.php";
    public static final String NOTIFICATION = POST + "notification.php";
}
