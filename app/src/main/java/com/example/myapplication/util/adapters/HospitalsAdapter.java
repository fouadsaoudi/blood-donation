package com.example.myapplication.util.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myapplication.R;
import com.example.myapplication.util.model.Hospital;

import java.util.ArrayList;

public class HospitalsAdapter extends RecyclerView.Adapter<HospitalsAdapter.MyViewHolder> {
    private ArrayList<Hospital> arrayList;
    private Context context;

    public HospitalsAdapter(ArrayList<Hospital> arrayList, Context context) {
        this.arrayList = arrayList;
        this.context = context;
    }

    @NonNull
    @Override
    public HospitalsAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new HospitalsAdapter.MyViewHolder(LayoutInflater.from(context).inflate(R.layout.layout_hospitals, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull HospitalsAdapter.MyViewHolder holder, int position) {
        Hospital hospital = arrayList.get(position);
        holder.name.setText(hospital.getName());
        holder.phone_number.setText(hospital.getPhoneNumber());
        holder.address.setText(hospital.getAddress());
    }

    @Override
    public int getItemCount() {
        return arrayList == null ? 0 : arrayList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView name, phone_number, address;
        MyViewHolder(@NonNull View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.tv_hospital_name);
            phone_number = itemView.findViewById(R.id.tv_hospital_number);
            address = itemView.findViewById(R.id.tv_hospital_address);

        }
    }
}
