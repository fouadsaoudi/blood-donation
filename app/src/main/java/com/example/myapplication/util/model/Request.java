package com.example.myapplication.util.model;

import java.io.Serializable;

public class Request implements Serializable {

    private String id, requestDate;
    private int plateletsNumber, unitsNumber, remainingPlatelets, remainingUnits;
    private Patient patient;
    private Hospital hospital;
    private boolean caseFinished;

    public Request(String id, String requestDate, boolean caseFinished, int plateletsNumber, int unitsNumber, int remainingPlatlets, int remainingUnits, Patient patient, Hospital hospital) {
        this.id = id;
        this.requestDate = requestDate;
        this.caseFinished = caseFinished;
        this.plateletsNumber = plateletsNumber;
        this.unitsNumber = unitsNumber;
        this.remainingPlatelets = remainingPlatlets;
        this.remainingUnits = remainingUnits;
        this.patient = patient;
        this.hospital = hospital;
    }

    public String getId() {
        return id;
    }

    public String getRequestDate() {
        return requestDate;
    }

    public int getPlateletsNumber() {
        return plateletsNumber;
    }

    public int getUnitsNumber() {
        return unitsNumber;
    }

    public int getRemainingPlatelets() {
        return remainingPlatelets;
    }

    public int getRemainingUnits() {
        return remainingUnits;
    }

    public Patient getPatient() {
        return patient;
    }

    public Hospital getHospital() {
        return hospital;
    }

    public boolean isCaseFinished() {
        return caseFinished;
    }

    public void setRemainingPlatelets(int remainingPlatelets) {
        this.remainingPlatelets = remainingPlatelets;
    }

    public void setRemainingUnits(int remainingUnits) {
        this.remainingUnits = remainingUnits;
    }

    public void setCaseFinished(boolean caseFinished) {
        this.caseFinished = caseFinished;
    }
}
