package com.example.myapplication.util.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

public class Donations implements Serializable, Parcelable {
    private String date_of_donation, phoneNumber, name;
    private int donationID, donated_platelets, donated_units;
    private Patient patient;

    public Donations(String date_of_donation, int donationID, int donated_platelets, int donated_units, Patient patient) {
        this.date_of_donation = date_of_donation;
        this.donationID = donationID;
        this.donated_platelets = donated_platelets;
        this.donated_units = donated_units;
        this.patient = patient;
    }

    public Donations(String date_of_donation, String phoneNumber, String name, int donated_platelets, int donated_units) {
        this.date_of_donation = date_of_donation;
        this.phoneNumber = phoneNumber;
        this.name = name;
        this.donated_platelets = donated_platelets;
        this.donated_units = donated_units;
    }

    protected Donations(Parcel in) {
        date_of_donation = in.readString();
        phoneNumber = in.readString();
        name = in.readString();
        donationID = in.readInt();
        donated_platelets = in.readInt();
        donated_units = in.readInt();
    }

    public static final Creator<Donations> CREATOR = new Creator<Donations>() {
        @Override
        public Donations createFromParcel(Parcel in) {
            return new Donations(in);
        }

        @Override
        public Donations[] newArray(int size) {
            return new Donations[size];
        }
    };

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public String getDate_of_donation() {
        return date_of_donation;
    }

    public void setDate_of_donation(String date_of_donation) {
        this.date_of_donation = date_of_donation;
    }

    public int getDonationID() {
        return donationID;
    }

    public void setDonationID(int donationID) {
        this.donationID = donationID;
    }

    public int getDonated_platelets() {
        return donated_platelets;
    }

    public void setDonated_platelets(int donated_platelets) {
        this.donated_platelets = donated_platelets;
    }

    public int getDonated_units() {
        return donated_units;
    }

    public void setDonated_units(int donated_units) {
        this.donated_units = donated_units;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(date_of_donation);
        dest.writeString(phoneNumber);
        dest.writeString(name);
        dest.writeInt(donationID);
        dest.writeInt(donated_platelets);
        dest.writeInt(donated_units);
    }
}
