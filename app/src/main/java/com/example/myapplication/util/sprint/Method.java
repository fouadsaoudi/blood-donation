package com.example.myapplication.util.sprint;

public enum Method {

    GET("GET"),
    POST("POST"),
    DELETE("DELETE"),
    PUT("PUT");

    private String method;

    Method(String method) {
        this.method = method;
    }

    public String getMethod() {
        return method;
    }
}
