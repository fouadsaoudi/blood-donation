package com.example.myapplication.util.listeners;

import com.example.myapplication.util.model.MyRequests;

public interface MyRequestsCallback {
    public void onClick(MyRequests myRequests);
}
