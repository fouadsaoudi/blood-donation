package com.example.myapplication.util.sprint.header;

public interface Header {
    String key();
    String value();
    void value(String value);
}
