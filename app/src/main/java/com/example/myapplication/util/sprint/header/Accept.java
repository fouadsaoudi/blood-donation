package com.example.myapplication.util.sprint.header;

public enum Accept implements Header {
    JSON("application/json");

    private String value;

    Accept(String value) {
        this.value = value;
    }

    @Override
    public String key() {
        return "Accept";
    }

    @Override
    public String value() {
        return value;
    }

    @Override
    public void value(String value) {
        this.value = value;
    }


}
