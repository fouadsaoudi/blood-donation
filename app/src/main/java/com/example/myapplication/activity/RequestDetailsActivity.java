package com.example.myapplication.activity;

import android.os.Bundle;
import android.widget.TextView;

import com.example.myapplication.R;
import com.example.myapplication.util.adapters.DonatedAdapter;
import com.example.myapplication.util.model.Donations;
import com.example.myapplication.util.model.MyRequests;

import java.util.ArrayList;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

public class RequestDetailsActivity extends AppCompatActivity {
    MyRequests myRequests;
    ArrayList<Donations> donations;
    TextView patientName, platelets, units, remainingPlatelets, remainingUnits, status, requestDate;
    RecyclerView whoDonated;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_request_details);
        myRequests = (MyRequests) getIntent().getSerializableExtra("request");
        donations = getIntent().getParcelableArrayListExtra("donations");
        init();
        DonatedAdapter donatedAdapter = new DonatedAdapter(RequestDetailsActivity.this, donations);
        whoDonated.setAdapter(donatedAdapter);
    }

    private void init() {
        patientName = findViewById(R.id.tv_patient_name);
        platelets = findViewById(R.id.tv_platelets);
        units = findViewById(R.id.tv_units);
        remainingPlatelets = findViewById(R.id.tv_remaining_platelets);
        remainingUnits = findViewById(R.id.tv_remaining_units);
        status = findViewById(R.id.tv_status);
        requestDate = findViewById(R.id.tv_request_date);
        whoDonated = findViewById(R.id.rv_who_donated);

        patientName.setText(myRequests.getFullName());
        platelets.setText(myRequests.getPlateletsNumber() + "");
        units.setText(myRequests.getUnitsNumber() + "");
        remainingPlatelets.setText(myRequests.getRemainingPlatelets() + "");
        remainingUnits.setText(myRequests.getRemainingUnits() + "");
        requestDate.setText(myRequests.getRequestDate());

        if (myRequests.isCaseFinished()) {
            status.setText(getString(R.string.Finished));
        } else {
            status.setText(getString(R.string.pending));
        }
    }
}
