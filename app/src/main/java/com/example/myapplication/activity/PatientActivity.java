package com.example.myapplication.activity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.myapplication.R;
import com.example.myapplication.util.SavedPreferences;
import com.example.myapplication.util.model.Hospital;
import com.example.myapplication.util.sprint.APIEndPoint;
import com.example.myapplication.util.sprint.Method;
import com.example.myapplication.util.sprint.Sprint;
import com.example.myapplication.util.sprint.SprintCallback;
import com.github.stephenvinouze.materialnumberpickercore.MaterialNumberPicker;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Objects;

public class PatientActivity extends AppCompatActivity {
    final Calendar myCalendar = Calendar.getInstance();
    MaterialNumberPicker platelets, units;
    Button submit;
    EditText fname, lname, contact_number, caseDescription;
    Spinner gender, bloodType, hospitalSpinner;
    TextView dob;
    String currentDateTime;
    ArrayList<Hospital> hospitalArrayList;
    ArrayAdapter<Hospital> hospitalArrayAdapter;
    private Hospital hospital;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_patient);

        init();

        submit.setOnClickListener(v -> {
            addRequest();
            finish();
        });
    }

    private void addRequest() {
        HashMap<String, String> map = new HashMap<>();
        map.put("fname", fname.getText().toString());
        map.put("lname", lname.getText().toString());
        map.put("contact_number", contact_number.getText().toString());
        map.put("dob", dob.getText().toString());
        map.put("case_description", caseDescription.getText().toString());
        map.put("gender", gender.getSelectedItem().toString());
        map.put("blood_type", bloodType.getSelectedItem().toString());
        map.put("users_id", SavedPreferences.getUserId(PatientActivity.this));
        map.put("req_date", currentDateTime);
        map.put("platelets_nb", platelets.getValue() + "");
        map.put("units_nb", units.getValue() + "");
        map.put("hospital", hospital.getId());
        Sprint.instance(Method.POST).request(APIEndPoint.ADD_REQUEST, map).execute(new SprintCallback() {
            @Override
            public void onSuccess(JSONObject data) {
                if (data != null) {
                    if (data.optString("result").equals("True")) {
//                        startActivity(new Intent(PatientActivity.this, MainActivity.class));
                        setResult(RESULT_OK);
                        finish();
                    } else {
                        Toast.makeText(PatientActivity.this, "Please fill all fields", Toast.LENGTH_LONG).show();
                    }
                }
            }

            @Override
            public void onFailure(JSONObject data, int statusCode) {
                Toast.makeText(PatientActivity.this, "Please Try Again", Toast.LENGTH_LONG).show();
                Log.e("patientError", data.optString("result"));
                Log.e("patientError", data.optString("message"));
            }

            @Override
            public void onException(Exception e) {
                Toast.makeText(PatientActivity.this, "Please Try Again", Toast.LENGTH_LONG).show();
                e.printStackTrace();
            }
        });
    }

    private void init() {
        hospitalSpinner = findViewById(R.id.spn_hospital);
        Sprint.instance().request(APIEndPoint.GET_ALL_HOSPITALS).execute(new SprintCallback() {
            @Override
            public void onSuccess(JSONObject data) {
                JSONArray hospitalsArray = data.optJSONArray("hospitals");
                if (hospitalsArray != null) {
                    hospitalArrayList = new ArrayList<>();
                    for (int i = 0; i < hospitalsArray.length(); i++) {
                        JSONObject current = hospitalsArray.optJSONObject(i);
                        if (current != null) {
                            hospitalArrayList.add(new Hospital(current.optString("id"),
                                    current.optString("name"),
                                    current.optString("phone_nb"),
                                    current.optString("email"),
                                    current.optString("address"),
                                    current.optString("area_id")));
                        }
                    }
                    populateHospitals();
                }
            }

            @Override
            public void onFailure(JSONObject data, int statusCode) {

            }

            @Override
            public void onException(Exception e) {
                e.printStackTrace();
            }
        });

        platelets = findViewById(R.id.platelets);
        units = findViewById(R.id.units);
        submit = findViewById(R.id.btn_patient_submit);
        fname = findViewById(R.id.edt_patient_fname);
        lname = findViewById(R.id.edt_patient_lname);
        contact_number = findViewById(R.id.edt_patient_contact_phone);
        caseDescription = findViewById(R.id.edt_patient_case_decription);
        gender = findViewById(R.id.spn_gender);
        bloodType = findViewById(R.id.spn_blood_type);
        dob = findViewById(R.id.txt_patient_dob);
        DatePickerDialog.OnDateSetListener dateSetListener = (view, year, month, dayOfMonth) -> {
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, month);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            updateLabel();
        };
        dob.setOnClickListener(v -> {
            // TODO Auto-generated method stub
            new DatePickerDialog(PatientActivity.this, dateSetListener, myCalendar
                    .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                    myCalendar.get(Calendar.DAY_OF_MONTH)).show();
        });

        Date c = Calendar.getInstance().getTime();

        SimpleDateFormat df = new SimpleDateFormat("YYYY-MM-dd hh:mm:ss", Locale.getDefault());
        currentDateTime = df.format(c);
    }

    private void populateHospitals() {
        hospitalArrayAdapter = new ArrayAdapter<Hospital>(this,
                android.R.layout.simple_spinner_item, hospitalArrayList) {
            @Override
            public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                View view = convertView == null ? LayoutInflater.from(PatientActivity.this).inflate(android.R.layout.simple_spinner_item, parent, false) : convertView;
                TextView name = view.findViewById(android.R.id.text1);
                String x = Objects.requireNonNull(getItem(position)).getName() + " - " + Objects.requireNonNull(getItem(position)).getPhoneNumber();
                name.setText(x);
                return view;
            }

            @Override
            public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                View view = convertView == null ? LayoutInflater.from(PatientActivity.this).inflate(android.R.layout.simple_spinner_item, parent, false) : convertView;
                TextView name = view.findViewById(android.R.id.text1);
                String x = Objects.requireNonNull(getItem(position)).getName() + " - " + Objects.requireNonNull(getItem(position)).getPhoneNumber();
                name.setText(x);
                view.setPadding(30, 30, 0, 30);
                return view;
            }
        };
        hospitalArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        try {
            hospitalSpinner.setAdapter(hospitalArrayAdapter);
        } catch (Exception e) {
            e.printStackTrace();
        }
        hospitalSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                hospital = hospitalArrayList.get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        hospitalSpinner.setSelection(0);
    }

    private void updateLabel() {
        String myFormat = "yyyy-MM-dd"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        dob.setText(sdf.format(myCalendar.getTime()));
    }
}
