package com.example.myapplication.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.example.myapplication.R;
import com.example.myapplication.util.SavedPreferences;
import com.example.myapplication.util.model.Area;
import com.example.myapplication.util.model.Hospital;
import com.example.myapplication.util.sprint.APIEndPoint;
import com.example.myapplication.util.sprint.Sprint;
import com.example.myapplication.util.sprint.SprintCallback;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class SplashActivity extends AppCompatActivity {
    ArrayList<Area> areaArrayList;
    ArrayList<Hospital> hospitalArrayList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_activiy);

        if (SavedPreferences.getCHECKED(SplashActivity.this)) {
            Sprint.instance().request(APIEndPoint.GET_ALL_HOSPITALS).execute(new SprintCallback() {
                @Override
                public void onSuccess(JSONObject data) {
                    JSONArray hospitalsArray = data.optJSONArray("hospitals");
                    if (hospitalsArray != null) {
                        hospitalArrayList = new ArrayList<>();
                        for (int i = 0; i < hospitalsArray.length(); i++) {
                            JSONObject current = hospitalsArray.optJSONObject(i);
                            if (current != null) {
                                hospitalArrayList.add(new Hospital(current.optString("id"),
                                        current.optString("name"),
                                        current.optString("phone_nb"),
                                        current.optString("email"),
                                        current.optString("address"),
                                        current.optString("area_id")));
                            }
                        }
                    }
                }

                @Override
                public void onFailure(JSONObject data, int statusCode) {

                }

                @Override
                public void onException(Exception e) {
                    e.printStackTrace();
                }
            });
            Sprint.instance().request(APIEndPoint.GET_AREAS).execute(new SprintCallback() {
                @Override
                public void onSuccess(JSONObject data) {
                    JSONArray areasJSonArray = data.optJSONArray("areas");
                    if (areasJSonArray != null) {
                        areaArrayList = new ArrayList<>();
                        for (int i = 0; i < areasJSonArray.length(); i++) {
                            JSONObject current = areasJSonArray.optJSONObject(i);
                            if (current != null) {
                                areaArrayList.add(new Area(current.optString("id"), current.optString("name")));
                            }
                        }
                        startActivity(new Intent(SplashActivity.this, MainActivity.class).putExtra("areas", areaArrayList).putExtra("hospitals", hospitalArrayList));
                        finish();
                    }
                }

                @Override
                public void onFailure(JSONObject data, int statusCode) {

                }

                @Override
                public void onException(Exception e) {
                    e.printStackTrace();
                }
            });
        } else {
            Sprint.instance().request(APIEndPoint.GET_ALL_HOSPITALS).execute(new SprintCallback() {
                @Override
                public void onSuccess(JSONObject data) {
                    JSONArray hospitalsArray = data.optJSONArray("hospitals");
                    if (hospitalsArray != null) {
                        hospitalArrayList = new ArrayList<>();
                        for (int i = 0; i < hospitalsArray.length(); i++) {
                            JSONObject current = hospitalsArray.optJSONObject(i);
                            if (current != null) {
                                hospitalArrayList.add(new Hospital(current.optString("id"),
                                        current.optString("name"),
                                        current.optString("phone_nb"),
                                        current.optString("email"),
                                        current.optString("address"),
                                        current.optString("area_id")));
                            }
                        }
                    }
                }

                @Override
                public void onFailure(JSONObject data, int statusCode) {

                }

                @Override
                public void onException(Exception e) {
                    e.printStackTrace();
                }
            });
            Sprint.instance().request(APIEndPoint.GET_AREAS).execute(new SprintCallback() {
                @Override
                public void onSuccess(JSONObject data) {
                    JSONArray areasJSonArray = data.optJSONArray("areas");
                    if (areasJSonArray != null) {
                        areaArrayList = new ArrayList<>();
                        for (int i = 0; i < areasJSonArray.length(); i++) {
                            JSONObject current = areasJSonArray.optJSONObject(i);
                            if (current != null) {
                                areaArrayList.add(new Area(current.optString("id"), current.optString("name")));
                            }
                        }
                        startActivity(new Intent(SplashActivity.this, LoginActivity.class).putExtra("areas", areaArrayList).putExtra("hospitals", hospitalArrayList));
                        finish();
                    }
                }

                public void onFailure(JSONObject data, int statusCode) {
                    Toast.makeText(SplashActivity.this, "Please Try Again", Toast.LENGTH_LONG).show();
                    Log.e("SplashError", data.optString("result"));
                    Log.e("SplashError", data.optString("message"));
                }

                @Override
                public void onException(Exception e) {
                    runOnUiThread(() -> Toast.makeText(SplashActivity.this, "make sure your connected", Toast.LENGTH_LONG).show());
                    e.printStackTrace();
                }
            });
        }
    }
}
