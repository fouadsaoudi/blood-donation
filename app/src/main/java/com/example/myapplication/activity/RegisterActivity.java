package com.example.myapplication.activity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.myapplication.R;
import com.example.myapplication.util.SavedPreferences;
import com.example.myapplication.util.model.Area;
import com.example.myapplication.util.model.Hospital;
import com.example.myapplication.util.sprint.APIEndPoint;
import com.example.myapplication.util.sprint.Method;
import com.example.myapplication.util.sprint.Sprint;
import com.example.myapplication.util.sprint.SprintCallback;
import com.google.firebase.iid.FirebaseInstanceId;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

public class RegisterActivity extends AppCompatActivity {

    Calendar myCalendar = Calendar.getInstance();
    EditText username, password, fname, lname, phoneNumber, address;
    Spinner genderSpinner, bloodTypeSpinner, areaSpinner;
    Button register;
    List<Area> areaList;
    ArrayAdapter<Area> areaAdapter;
    TextView dateOfBirth;
    ArrayList<Area> areasArrayList;
    ArrayList<Hospital> hospitalArrayList;

    private Area selectedArea;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        init();

        register.setOnClickListener(v -> {
            String passwordstr = password.getText().toString();
            String emailstr = username.getText().toString();
            String phonestr = phoneNumber.getText().toString();
            View focus = null;
            boolean cancel = false;
            if (!isEmailValid(username)) {
                focus = username;
                cancel = true;
                username.setError(getString(R.string.error_invalid_email));
            }
            if (TextUtils.isEmpty(emailstr.trim())) {
                focus = username;
                username.setError(getString(R.string.error_field_required));
                cancel = true;
            }
            if (!isPasswordValid(password)) {
                cancel = true;
                focus = password;
                password.setError(getString(R.string.error_invalid_password));
            }
            if (TextUtils.isEmpty(passwordstr.trim())) {
                focus = password;
                cancel = true;
                password.setError(getString(R.string.error_field_required));
            }
            if (!isPhoneNumberValid(phoneNumber)) {
                cancel = true;
                focus = phoneNumber;
                phoneNumber.setError(getString(R.string.error_invalid_phone_number));
            }
            if (TextUtils.isEmpty(phonestr.trim())) {
                focus = phoneNumber;
                cancel = true;
                phoneNumber.setError(getString(R.string.error_field_required));
            }

            if (cancel)
                focus.requestFocus();
            else {
                register();
            }
        });

    }

    private void register() {
        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(command -> {
            HashMap<String, String> map = new HashMap<>();
            map.put("username", username.getText().toString());
            map.put("password", password.getText().toString());
            map.put("dob", dateOfBirth.getText().toString());
            map.put("fname", fname.getText().toString());
            map.put("lname", lname.getText().toString());
            map.put("phone_number", phoneNumber.getText().toString());
            map.put("address", address.getText().toString());
            map.put("gender", genderSpinner.getSelectedItem().toString());
            map.put("blood_type", bloodTypeSpinner.getSelectedItem().toString());
            map.put("area", selectedArea.getId());
            map.put("device_token", command.getToken());
            Sprint.instance(Method.POST).request(APIEndPoint.REGISTER, map).execute(new SprintCallback() {
                @Override
                public void onSuccess(JSONObject data) {
                    if (data.optString("result").equals("True")) {
                        SavedPreferences.setDOB(RegisterActivity.this, dateOfBirth.getText().toString());
                        SavedPreferences.setFullName(RegisterActivity.this, fname.getText().toString() + " " + lname.getText().toString());
                        SavedPreferences.setNumber(RegisterActivity.this, phoneNumber.getText().toString());
                        SavedPreferences.setAddress(RegisterActivity.this, address.getText().toString());
                        SavedPreferences.setGenger(RegisterActivity.this, genderSpinner.getSelectedItem().toString());
                        SavedPreferences.setbloodType(RegisterActivity.this, bloodTypeSpinner.getSelectedItem().toString());
                        SavedPreferences.setUserId(RegisterActivity.this, data.optString("id"));
                        SavedPreferences.setUserName(RegisterActivity.this, username.getText().toString());
                        SavedPreferences.setPASSWORD(RegisterActivity.this, password.getText().toString());
                        SavedPreferences.setArea(RegisterActivity.this, areaSpinner.getSelectedItem().toString());
                        startActivity(new Intent(RegisterActivity.this, MainActivity.class).putExtra("hospitals", hospitalArrayList));
                        finish();
                    } else {
                        Toast.makeText(RegisterActivity.this, "Something went wrong", Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void onFailure(JSONObject data, int statusCode) {
                    Toast.makeText(RegisterActivity.this, "Please Try Again", Toast.LENGTH_LONG).show();
                    Log.e("registerError", data.optString("result"));
                    Log.e("registerError", data.optString("message"));
                }

                @Override
                public void onException(Exception e) {
                    Toast.makeText(RegisterActivity.this, "Please Try Again", Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                }
            });
        });
    }

    private void init() {
        Sprint.instance().request(APIEndPoint.GET_ALL_HOSPITALS).execute(new SprintCallback() {
            @Override
            public void onSuccess(JSONObject data) {
                JSONArray hospitalsArray = data.optJSONArray("hospitals");
                if (hospitalsArray != null) {
                    hospitalArrayList = new ArrayList<>();
                    for (int i = 0; i < hospitalsArray.length(); i++) {
                        JSONObject current = hospitalsArray.optJSONObject(i);
                        if (current != null) {
                            hospitalArrayList.add(new Hospital(current.optString("id"),
                                    current.optString("name"),
                                    current.optString("phone_nb"),
                                    current.optString("email"),
                                    current.optString("address"),
                                    current.optString("area_id")));
                        }
                    }
                }
            }

            @Override
            public void onFailure(JSONObject data, int statusCode) {

            }

            @Override
            public void onException(Exception e) {
                e.printStackTrace();
            }
        });

        Sprint.instance().request(APIEndPoint.GET_AREAS).execute(new SprintCallback() {
            @Override
            public void onSuccess(JSONObject data) {
                JSONArray areasJSonArray = data.optJSONArray("areas");
                if (areasJSonArray != null) {
                    areasArrayList = new ArrayList<>();
                    for (int i = 0; i < areasJSonArray.length(); i++) {
                        JSONObject current = areasJSonArray.optJSONObject(i);
                        if (current != null) {
                            areasArrayList.add(new Area(current.optString("id"), current.optString("name")));
                        }
                    }
                }
            }

            @Override
            public void onFailure(JSONObject data, int statusCode) {

            }

            @Override
            public void onException(Exception e) {
                e.printStackTrace();
            }
        });

        username = findViewById(R.id.edt_email);
        password = findViewById(R.id.edt_password);
        dateOfBirth = findViewById(R.id.Birthday);
        fname = findViewById(R.id.edt_fname);
        lname = findViewById(R.id.edt_lname);
        phoneNumber = findViewById(R.id.edt_phone_number);
        address = findViewById(R.id.edt_address);
        genderSpinner = findViewById(R.id.spn_gender);
        bloodTypeSpinner = findViewById(R.id.spn_blood_type);
        areaSpinner = findViewById(R.id.spn_area);
        register = findViewById(R.id.btn_register);

        DatePickerDialog.OnDateSetListener dateSetListener = (view, year, month, dayOfMonth) -> {
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, month);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            updateLabel();
        };

        dateOfBirth.setOnClickListener(v -> {
            // TODO Auto-generated method stub
            new DatePickerDialog(RegisterActivity.this, dateSetListener, myCalendar
                    .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                    myCalendar.get(Calendar.DAY_OF_MONTH)).show();
        });

        areaList = getIntent().getParcelableArrayListExtra("areas");

        areaAdapter = new ArrayAdapter<Area>(this,
                android.R.layout.simple_spinner_item, areaList) {
            @Override
            public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                View view = convertView == null ? LayoutInflater.from(RegisterActivity.this).inflate(android.R.layout.simple_spinner_item, parent, false) : convertView;
                TextView name = view.findViewById(android.R.id.text1);
                name.setText(getItem(position).getName());
                return view;
            }

            @Override
            public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                View view = convertView == null ? LayoutInflater.from(RegisterActivity.this).inflate(android.R.layout.simple_spinner_item, parent, false) : convertView;
                TextView name = view.findViewById(android.R.id.text1);
                name.setText(getItem(position).getName());
                view.setPadding(30, 30, 0, 30);
                return view;
            }
        };

        areaAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        try {
            areaSpinner.setAdapter(areaAdapter);
        } catch (Exception e) {
            e.printStackTrace();
        }

        areaSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedArea = areaList.get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        areaSpinner.setSelection(0);
    }

    private void updateLabel() {
        String myFormat = "yyyy-MM-dd"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.getDefault());
        dateOfBirth.setText(sdf.format(myCalendar.getTime()));
    }

    private boolean isPasswordValid(EditText password) {
        return password.getText().toString().length() > 6;
    }

    private boolean isEmailValid(EditText email) {
        return email.getText().toString().contains("@") && email.getText().toString().contains(".com");
    }

    private boolean isPhoneNumberValid(EditText phoneNumber) {
        return phoneNumber.getText().toString().length() > 6;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
