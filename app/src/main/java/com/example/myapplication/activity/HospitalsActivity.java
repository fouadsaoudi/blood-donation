package com.example.myapplication.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import com.example.myapplication.R;
import com.example.myapplication.util.adapters.HospitalsAdapter;
import com.example.myapplication.util.model.Hospital;
import com.example.myapplication.util.sprint.APIEndPoint;
import com.example.myapplication.util.sprint.Sprint;
import com.example.myapplication.util.sprint.SprintCallback;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class HospitalsActivity extends AppCompatActivity {
    ArrayList<Hospital> hospitalArrayList;
    RecyclerView hospitalsRecyclerView;
    HospitalsAdapter hospitalsAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hospitals);
        hospitalsRecyclerView = findViewById(R.id.rv_hospitals);

        Sprint.instance().request(APIEndPoint.GET_ALL_HOSPITALS).execute(new SprintCallback() {
            @Override
            public void onSuccess(JSONObject data) {
                JSONArray hospitalsArray = data.optJSONArray("hospitals");
                if (hospitalsArray != null) {
                    hospitalArrayList = new ArrayList<>();
                    for (int i = 0; i < hospitalsArray.length(); i++) {
                        JSONObject current = hospitalsArray.optJSONObject(i);
                        if (current != null) {
                            hospitalArrayList.add(new Hospital(current.optString("id"),
                                    current.optString("name"),
                                    current.optString("phone_nb"),
                                    current.optString("email"),
                                    current.optString("address"),
                                    current.optString("area_id")));
                        }
                    }
                    hospitalsAdapter = new HospitalsAdapter(hospitalArrayList, HospitalsActivity.this);
                    hospitalsRecyclerView.setAdapter(hospitalsAdapter);
                }
            }

            @Override
            public void onFailure(JSONObject data, int statusCode) {

            }

            @Override
            public void onException(Exception e) {
                e.printStackTrace();
            }
        });

    }
}
