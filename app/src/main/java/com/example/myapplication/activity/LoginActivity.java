package com.example.myapplication.activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.myapplication.R;
import com.example.myapplication.util.SavedPreferences;
import com.example.myapplication.util.model.Area;
import com.example.myapplication.util.model.Hospital;
import com.example.myapplication.util.sprint.APIEndPoint;
import com.example.myapplication.util.sprint.Method;
import com.example.myapplication.util.sprint.Sprint;
import com.example.myapplication.util.sprint.SprintCallback;
import com.google.firebase.iid.FirebaseInstanceId;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class LoginActivity extends AppCompatActivity {
    EditText password, username;
    Button login;
    CheckBox checkBox;
    TextView register, guest;

    ArrayList<Area> areasArrayList;
    ArrayList<Hospital> hospitalArrayList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        init();

        guest.setOnClickListener(v -> {
            startActivity(new Intent(LoginActivity.this, GuestActivity.class).putExtra("areas", areasArrayList));
        });

        register.setOnClickListener(v -> {
            startActivity(new Intent(LoginActivity.this, RegisterActivity.class).putExtra("areas", areasArrayList).putExtra("hospitals", hospitalArrayList));
        });

        checkBox.setOnCheckedChangeListener((buttonView, isChecked) -> {
            SavedPreferences.setCHECKED(LoginActivity.this, isChecked);
        });

        login.setOnClickListener(v -> attemptLogin());
    }

    private void init() {
        username = findViewById(R.id.edt_user_name);
        password = findViewById(R.id.edt_password);
        login = findViewById(R.id.btn_login);
        checkBox = findViewById(R.id.checkbox);
        register = findViewById(R.id.tv_register);
        guest = findViewById(R.id.tv_guest);
        areasArrayList = getIntent().getParcelableArrayListExtra("areas");
        hospitalArrayList = getIntent().getParcelableArrayListExtra("hospitals");
    }

    private boolean isPasswordValid(EditText password) {
        return password.getText().toString().length() > 6;
    }

    private boolean isEmailValid(EditText email) {
        return email.getText().toString().contains("@") && email.getText().toString().contains(".com");
    }

    public void attemptLogin() {
        String passwordstr = password.getText().toString();
        String emailstr = username.getText().toString();
        View focus = null;
        boolean cancel = false;
        if (!isEmailValid(username)) {
            focus = username;
            cancel = true;
            username.setError(getString(R.string.error_invalid_email));
        }
        if (TextUtils.isEmpty(emailstr.trim())) {
            focus = username;
            username.setError(getString(R.string.error_field_required));
            cancel = true;
        }
        if (!isPasswordValid(password)) {
            cancel = true;
            focus = password;
            password.setError(getString(R.string.error_invalid_password));
        }
        if (TextUtils.isEmpty(passwordstr.trim())) {
            focus = password;
            cancel = true;
            password.setError(getString(R.string.error_field_required));
        }
        if (cancel)
            focus.requestFocus();
        else {
            login();
        }
    }

    public void login() {
        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(command -> {
            HashMap<String, String> map = new HashMap<>();
            String token = command.getToken();
            map.put("device_token", token);
            map.put("username", username.getText().toString());
            map.put("password", password.getText().toString());
            Sprint.instance(Method.POST).request(APIEndPoint.LOGIN, map).execute(new SprintCallback() {
                @Override
                public void onSuccess(JSONObject data) {
                    SavedPreferences.setDOB(LoginActivity.this, data.optString("DOB"));
                    SavedPreferences.setFullName(LoginActivity.this, data.optString("fname") + " " + data.optString("lname"));
                    SavedPreferences.setNumber(LoginActivity.this, data.optString("phone_number"));
                    SavedPreferences.setAddress(LoginActivity.this, data.optString("Address"));
                    SavedPreferences.setGenger(LoginActivity.this, data.optString("gender"));
                    SavedPreferences.setbloodType(LoginActivity.this, data.optString("blood_type"));
                    SavedPreferences.setUserId(LoginActivity.this, data.optString("id"));
                    SavedPreferences.setUserName(LoginActivity.this, data.optString("username"));
                    SavedPreferences.setPASSWORD(LoginActivity.this, data.optString("password"));
                    SavedPreferences.setArea(LoginActivity.this, data.optString("area"));
                    startActivity(new Intent(LoginActivity.this, MainActivity.class).putExtra("hospitals", hospitalArrayList).putExtra("areas", areasArrayList));
                    finish();
                }

                @Override
                public void onFailure(JSONObject data, int statusCode) {
                    Toast.makeText(LoginActivity.this, "Please Try Again", Toast.LENGTH_LONG).show();
                    Log.e("loginError", data.optString("result"));
                    Log.e("loginError", data.optString("message"));
                }

                @Override
                public void onException(Exception e) {
//                    Looper.prepare();
                    e.printStackTrace();
                    Toast.makeText(LoginActivity.this, "Please Try Again", Toast.LENGTH_LONG).show();
                }
            });
        });

    }

}
