package com.example.myapplication.activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.example.myapplication.R;
import com.example.myapplication.SettingActivity;
import com.example.myapplication.util.SavedPreferences;
import com.example.myapplication.util.adapters.RequestsAdapter;
import com.example.myapplication.util.model.Area;
import com.example.myapplication.util.model.Hospital;
import com.example.myapplication.util.model.Patient;
import com.example.myapplication.util.model.Request;
import com.example.myapplication.util.sprint.APIEndPoint;
import com.example.myapplication.util.sprint.Sprint;
import com.example.myapplication.util.sprint.SprintCallback;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    ArrayList<Hospital> hospitalArrayList;
    ArrayList<Area> areasArrayList;
    SwipeRefreshLayout swiperefresh;
    SwipeRefreshLayout mSwipeRefreshLayout;
    private RecyclerView requestsRecycler;
    ArrayList<Request> requestArrayList;
    RequestsAdapter requestsAdapter;
    private FloatingActionButton addRequest;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        init();

        requestArrayList = new ArrayList<>();
//        getAllRequests();
        mSwipeRefreshLayout = findViewById(R.id.swiperefresh);
        mSwipeRefreshLayout.setRefreshing(true);
        getAllRequests();
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                requestArrayList.clear();
                requestsAdapter.notifyDataSetChanged();
                requestsRecycler.setAdapter(null);
                getAllRequests();
            }
        });
        mSwipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary,
                android.R.color.holo_green_dark,
                android.R.color.holo_orange_dark,
                android.R.color.holo_blue_dark);

        if (SavedPreferences.getRINGTONE(this)){
            Toast.makeText(this, "your notifications are muted", Toast.LENGTH_LONG).show();
        }
    }

    private void init() {
        areasArrayList = getIntent().getParcelableArrayListExtra("areas");
        hospitalArrayList = getIntent().getParcelableArrayListExtra("hospitals");

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        TextView userName = navigationView.getHeaderView(0).findViewById(R.id.tv_user_name);
        TextView email = navigationView.getHeaderView(0).findViewById(R.id.tv_email);
        userName.setText(SavedPreferences.getFullName(MainActivity.this));
        email.setText(SavedPreferences.getUserName(MainActivity.this));
        navigationView.setNavigationItemSelectedListener(this);

        requestsRecycler = findViewById(R.id.rv_requests);
        addRequest = findViewById(R.id.fab_add_request);
        addRequest.setOnClickListener(v -> {
            startActivityForResult(new Intent(MainActivity.this, PatientActivity.class).putExtra("hospitals", hospitalArrayList), 1000);
        });
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_logout) {
            SavedPreferences.clear(MainActivity.this);
            startActivity(new Intent(MainActivity.this, SplashActivity.class));
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_requests) {
            startActivity(new Intent(MainActivity.this, MyRequestsActivity.class));
//            // Handle the camera action
        } else if (id == R.id.nav_donations) {
            startActivity(new Intent(MainActivity.this, allDonationsActivity.class));
        } else if (id == R.id.nav_blood_search) {
            startActivity(new Intent(MainActivity.this, BloodSearchActivity.class).putExtra("areas", areasArrayList));
        }else if (id == R.id.nav_hospital_search) {
            startActivity(new Intent(MainActivity.this, HospitalsActivity.class));
        }else if (id == R.id.nav_settings){
            startActivity(new Intent(MainActivity.this, SettingActivity.class));
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void getAllRequests() {

        Sprint.instance().request(APIEndPoint.GET_ALL_REQUEST).execute(new SprintCallback() {
            @Override
            public void onSuccess(JSONObject data) {
                JSONArray requestArray = data.optJSONArray("requests");
                if (requestArray != null) {
                    for (int i = 0; i < requestArray.length(); i++) {
                        JSONObject current = requestArray.optJSONObject(i);
                        if (current != null) {
                            JSONObject jsonHospital = current.optJSONObject("hospital");
                            JSONObject jsonPatient = current.optJSONObject("patient");
                            Hospital hospital = new Hospital(jsonHospital.optString("id"),
                                    jsonHospital.optString("name"),
                                    jsonHospital.optString("phone_nb"),
                                    jsonHospital.optString("email"),
                                    jsonHospital.optString("address"),
                                    jsonHospital.optString("area_id"));
                            Patient patient = new Patient(jsonPatient.optString("id"),
                                    jsonPatient.optString("fname"),
                                    jsonPatient.optString("lname"),
                                    jsonPatient.optString("contact_nb"),
                                    jsonPatient.optString("DOB"),
                                    jsonPatient.optString("case_description"),
                                    jsonPatient.optString("gender"),
                                    jsonPatient.optString("blood_type"),
                                    jsonPatient.optString("users_id"));
                            requestArrayList.add(new Request(current.optString("id"),
                                    current.optString("request_date"),
                                    current.optBoolean("case_finished"),
                                    current.optInt("platelets_nb"),
                                    current.optInt("unit_nb"),
                                    current.optInt("remaining_platelets"),
                                    current.optInt("remaining_units"),
                                    patient,
                                    hospital));
                        }
                    }
                    mSwipeRefreshLayout.setRefreshing(false);
                    requestsAdapter = new RequestsAdapter(requestArrayList, MainActivity.this, request -> {
                        startActivity(new Intent(MainActivity.this, DonateActivity.class).putExtra("requestDetails", request));
                    });
                    requestsAdapter.notifyDataSetChanged();
                    requestsRecycler.setAdapter(requestsAdapter);
                }
            }

            @Override
            public void onFailure(JSONObject data, int statusCode) {
                Toast.makeText(MainActivity.this, "Please Try Again", Toast.LENGTH_LONG).show();
                Log.e("requestError", data.optString("result"));
                Log.e("requestError", data.optString("message"));
                mSwipeRefreshLayout.setRefreshing(false);
            }

            @Override
            public void onException(Exception e) {
                Toast.makeText(MainActivity.this, "Please Try Again", Toast.LENGTH_LONG).show();
                e.printStackTrace();
                mSwipeRefreshLayout.setRefreshing(false);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == 1000 && resultCode == RESULT_OK) {
            mSwipeRefreshLayout.setRefreshing(true);
            requestArrayList.clear();
            requestsAdapter.notifyDataSetChanged();
            requestsRecycler.setAdapter(null);
            getAllRequests();
        }
        else {
            mSwipeRefreshLayout.setRefreshing(true);
            requestArrayList.clear();
            requestsAdapter.notifyDataSetChanged();
            requestsRecycler.setAdapter(null);
            getAllRequests();
        }
    }
}
