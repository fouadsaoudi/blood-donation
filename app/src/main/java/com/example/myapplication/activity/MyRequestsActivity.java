package com.example.myapplication.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.util.Log;
import android.widget.Toast;

import com.example.myapplication.R;
import com.example.myapplication.util.SavedPreferences;
import com.example.myapplication.util.adapters.MyRequestsAdapter;
import com.example.myapplication.util.listeners.MyRequestsCallback;
import com.example.myapplication.util.model.Donations;
import com.example.myapplication.util.model.MyRequests;
import com.example.myapplication.util.sprint.APIEndPoint;
import com.example.myapplication.util.sprint.Method;
import com.example.myapplication.util.sprint.Sprint;
import com.example.myapplication.util.sprint.SprintCallback;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

public class MyRequestsActivity extends AppCompatActivity {
    RecyclerView myRequestsRecyclerView;
    ArrayList<MyRequests> myRequestsArrayList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_requests);
        myRequestsRecyclerView = findViewById(R.id.rv_my_requests);
        getMyRequests();
    }

    private void getMyRequests() {
        HashMap<String, String> map = new HashMap<>();
        map.put("user_id", SavedPreferences.getUserId(MyRequestsActivity.this));
        Sprint.instance(Method.POST).request(APIEndPoint.MY_REQUESTS, map).execute(new SprintCallback() {
            @Override
            public void onSuccess(JSONObject data) {
                JSONArray myRequestsJSONArray = data.optJSONArray("own_request");
                if (myRequestsJSONArray != null) {
                    myRequestsArrayList = new ArrayList<>(myRequestsJSONArray.length());
                    for (int i = 0; i < myRequestsJSONArray.length(); i++) {
                        ArrayList<Donations> donationsArrayList = new ArrayList<>();
                        JSONObject current = myRequestsJSONArray.optJSONObject(i);

                        JSONArray donations = current.optJSONArray("donations");
                        if (donations != null) {
                            for (int n = 0; n < donations.length(); n++) {
                                JSONObject donation = donations.optJSONObject(n);
                                donationsArrayList.add(new Donations(donation.optString("date_of_donation"),
                                        donation.optString("phone_number"),
                                        donation.optString("name"),
                                        donation.optInt("donated_platelets"),
                                        donation.optInt("donated_units")
                                ));
                            }
                        }
                        //adding the donations list to request
                        myRequestsArrayList.add(new MyRequests(current.optString("id"),
                                current.optString("request_date"),
                                current.optString("name"),
                                current.optInt("platelets_nb"),
                                current.optInt("unit_nb"),
                                current.optInt("remaining_platelets"),
                                current.optInt("remaining_units"),
                                current.optBoolean("case_finished"),
                                donationsArrayList
                        ));
                    }

                    MyRequestsAdapter myRequestsAdapter = new MyRequestsAdapter(myRequestsArrayList, MyRequestsActivity.this, myRequests ->
                            startActivity(new Intent(MyRequestsActivity.this, RequestDetailsActivity.class)
                                    .putExtra("request", (Serializable) myRequests)
                                    .putExtra("donations", myRequests.getDonationsArrayList())));
                    myRequestsRecyclerView.setAdapter(myRequestsAdapter);
                }
            }

            @Override
            public void onFailure(JSONObject data, int statusCode) {
                Log.e("Sprint", data.toString());
            }

            @Override
            public void onException(Exception e) {
                e.printStackTrace();
            }
        });
    }
}
