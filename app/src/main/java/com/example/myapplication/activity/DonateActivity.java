package com.example.myapplication.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.myapplication.R;
import com.example.myapplication.util.SavedPreferences;
import com.example.myapplication.util.model.Request;
import com.example.myapplication.util.sprint.APIEndPoint;
import com.example.myapplication.util.sprint.Method;
import com.example.myapplication.util.sprint.Sprint;
import com.example.myapplication.util.sprint.SprintCallback;
import com.github.stephenvinouze.materialnumberpickercore.MaterialNumberPicker;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

public class DonateActivity extends AppCompatActivity {
    final Calendar myCalendar = Calendar.getInstance();
    Request request;
    TextView fullName, contactNumber, dob, caseDescription,
    gender, bloodType, remainingPlatelets, remainingUnits,
    hosName, hosNumber,hosAddress,
    requestDate;
    String currentDateTime;
    MaterialNumberPicker platelets, units;
    Button donate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_donate);
        request = (Request) getIntent().getSerializableExtra("requestDetails");
        init();

        Date c = Calendar.getInstance().getTime();
        Toast.makeText(DonateActivity.this, "Current time => " + c, Toast.LENGTH_LONG).show();

        SimpleDateFormat df = new SimpleDateFormat("YYYY-MM-dd hh:mm:ss", Locale.getDefault());
        currentDateTime = df.format(c);
        donate.setOnClickListener(v -> {
            submitDonation();
        });
    }

    private void submitDonation() {
        HashMap<String, String> map = new HashMap<>();
        map.put("donated_platelets",platelets.getValue()+"");
        map.put("donated_units",units.getValue()+"");
        map.put("date_of_donation",currentDateTime);
        map.put("requests_id",request.getId());
        map.put("users_id", SavedPreferences.getUserId(DonateActivity.this));
        Sprint.instance(Method.POST).request(APIEndPoint.DONATE, map).execute(new SprintCallback() {
            @Override
            public void onSuccess(JSONObject data) {
                if (data.optString("result").equals("true")){
                    Toast.makeText(DonateActivity.this, "Thank you for your donation", Toast.LENGTH_LONG).show();
                    startActivity(new Intent(DonateActivity.this, MainActivity.class));
                    finish();
                }
            }

            @Override
            public void onFailure(JSONObject data, int statusCode) {

            }

            @Override
            public void onException(Exception e) {

            }
        });
    }

    private void init() {
        fullName = findViewById(R.id.tv_full_name);
        contactNumber = findViewById(R.id.tv_contact_number);
        dob = findViewById(R.id.tv_dob);
        caseDescription = findViewById(R.id.tv_case_description);
        gender = findViewById(R.id.tv_gender);
        bloodType = findViewById(R.id.tv_blood_type);
        remainingPlatelets = findViewById(R.id.remaining_platelets);
        remainingUnits = findViewById(R.id.remaining_units);
        hosName = findViewById(R.id.tv_hospital_name);
        hosNumber = findViewById(R.id.tv_hospital_number);
        hosAddress = findViewById(R.id.tv_hospital_address);
        requestDate = findViewById(R.id.tv_request_date);
        platelets = findViewById(R.id.number_picker_platelets);
        units = findViewById(R.id.number_picker_units);
        donate = findViewById(R.id.btn_donate);

        fullName.setText(request.getPatient().getFullName());
        contactNumber.setText(request.getPatient().getContactNumber());
        dob.setText(request.getPatient().getDob());
        caseDescription.setText(request.getPatient().getCaseDescription());
        gender.setText(request.getPatient().getGender());
        bloodType.setText(request.getPatient().getBloodType());
        remainingUnits.setText(request.getUnitsNumber()+"");
        remainingPlatelets.setText(request.getPlateletsNumber()+"");
        hosName.setText(request.getHospital().getName());
        hosNumber.setText(request.getHospital().getPhoneNumber());
        hosAddress.setText(request.getHospital().getAddress());
        requestDate.setText(request.getRequestDate());
        platelets.setMaxValue(request.getRemainingPlatelets());
        units.setMaxValue(request.getRemainingUnits());
    }
}
