package com.example.myapplication.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.example.myapplication.R;
import com.example.myapplication.util.SavedPreferences;
import com.example.myapplication.util.adapters.DonationsAdapter;
import com.example.myapplication.util.model.Donations;
import com.example.myapplication.util.model.Patient;
import com.example.myapplication.util.sprint.APIEndPoint;
import com.example.myapplication.util.sprint.Method;
import com.example.myapplication.util.sprint.Sprint;
import com.example.myapplication.util.sprint.SprintCallback;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class allDonationsActivity extends AppCompatActivity {

    private ArrayList<Donations> donationsArrayList;
    private RecyclerView donationsRecyclerView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_donations);
        donationsRecyclerView = findViewById(R.id.rv_donations);
        getAllDonations();
    }
    private void getAllDonations() {
        HashMap<String, String> map = new HashMap<>();
        map.put("user_id", SavedPreferences.getUserId(allDonationsActivity.this));
        Sprint.instance(Method.POST).request(APIEndPoint.DONATIONS, map).execute(new SprintCallback() {
            @Override
            public void onSuccess(JSONObject data) {
                donationsArrayList = new ArrayList<>();
                JSONArray donationsJSONARRAY = data.optJSONArray("donations");
                if (donationsJSONARRAY != null){
                    for (int i = 0; i < donationsJSONARRAY.length(); i++){
                        JSONObject current = donationsJSONARRAY.optJSONObject(i);
                        if (current != null){
                            JSONObject patientJson = current.optJSONObject("patient_info");
                            Patient patient = new Patient(patientJson.optString("fname"),
                                    patientJson.optString("lname"),
                                    patientJson.optString("contact_nb"),
                                    patientJson.optString("case_description"));
                            donationsArrayList.add(new Donations(current.optString("date_of_donation"),
                                    current.optInt("id"),
                                    current.optInt("donated_platelets"),
                                    current.optInt("donated_units"), patient));
                        }
                    }
                }
                DonationsAdapter donationsAdapter = new DonationsAdapter(donationsArrayList, allDonationsActivity.this);
                donationsRecyclerView.setAdapter(donationsAdapter);
            }

            public void onFailure(JSONObject data, int statusCode) {
                Toast.makeText(allDonationsActivity.this, "Please Try Again", Toast.LENGTH_LONG).show();
                Log.e("donationError", data.optString("result"));
                Log.e("donationError", data.optString("message"));
            }

            @Override
            public void onException(Exception e) {
                Toast.makeText(allDonationsActivity.this, "Please Try Again", Toast.LENGTH_LONG).show();
                e.printStackTrace();
            }
        });
    }
}
