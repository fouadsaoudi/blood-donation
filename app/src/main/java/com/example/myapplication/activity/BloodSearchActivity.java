package com.example.myapplication.activity;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.myapplication.R;
import com.example.myapplication.util.adapters.DonerAdapter;
import com.example.myapplication.util.model.Area;
import com.example.myapplication.util.model.Doner;
import com.example.myapplication.util.sprint.APIEndPoint;
import com.example.myapplication.util.sprint.Method;
import com.example.myapplication.util.sprint.Sprint;
import com.example.myapplication.util.sprint.SprintCallback;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

public class BloodSearchActivity extends AppCompatActivity {

    RecyclerView donerRecyclerView;
    Spinner area, bloodType;
    Button search;
    ArrayAdapter<Area> areaAdapter;
    ArrayList<Area> areaList;
    Area selectedArea;
    DonerAdapter donationsAdapter;
    ArrayList<Doner> donerArrayList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_blood_search);
        init();
        search.setOnClickListener(v -> {
            getDoners();
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        init();
        search.setOnClickListener(v -> {
            getDoners();
        });
    }

    private void getDoners() {
        HashMap<String, String> map = new HashMap<>();
        map.put("area", selectedArea.getId());
        map.put("blood_type", (String) bloodType.getSelectedItem());
        Sprint.instance(Method.POST).request(APIEndPoint.BLOOD_SEARCH, map).execute(new SprintCallback() {
            @Override
            public void onSuccess(JSONObject data) {
                JSONArray doners = data.optJSONArray("doners");
                if (doners != null) {
                    if (!donerArrayList.isEmpty())
                        donationsAdapter.notifyItemRangeRemoved(0, donerArrayList.size());
                    donerArrayList.clear();
                    for (int i = 0; i < doners.length(); i++) {
                        JSONObject current = doners.optJSONObject(i);
                        donerArrayList.add(new Doner(current.optString("name"), current.optString("phone_number")));
                        donationsAdapter.notifyItemInserted(i);
                    }
                    Toast.makeText(BloodSearchActivity.this, donerArrayList.size() + " Donne's available", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(JSONObject data, int statusCode) {
                donerArrayList.clear();
                donationsAdapter.notifyDataSetChanged();
                Toast.makeText(BloodSearchActivity.this, data.optString("result"), Toast.LENGTH_LONG).show();
            }

            @Override
            public void onException(Exception e) {

            }
        });
    }

    private void init() {
        donerRecyclerView = findViewById(R.id.rv_doners);
        area = findViewById(R.id.spn_area);
        bloodType = findViewById(R.id.spn_blood_type);
        search = findViewById(R.id.btn_search);

        Sprint.instance().request(APIEndPoint.GET_AREAS).execute(new SprintCallback() {
            @Override
            public void onSuccess(JSONObject data) {
                JSONArray areasJSonArray = data.optJSONArray("areas");
                if (areasJSonArray != null) {
                    areaList = new ArrayList<>();
                    for (int i = 0; i < areasJSonArray.length(); i++) {
                        JSONObject current = areasJSonArray.optJSONObject(i);
                        if (current != null) {
                            areaList.add(new Area(current.optString("id"), current.optString("name")));
                        }
                    }

                    areaAdapter = new ArrayAdapter<Area>(BloodSearchActivity.this,
                            android.R.layout.simple_spinner_item, areaList) {
                        @Override
                        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                            View view = convertView == null ? LayoutInflater.from(BloodSearchActivity.this).inflate(android.R.layout.simple_spinner_item, parent, false) : convertView;
                            TextView name = view.findViewById(android.R.id.text1);
                            name.setText(getItem(position).getName());
                            return view;
                        }

                        @Override
                        public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                            View view = convertView == null ? LayoutInflater.from(BloodSearchActivity.this).inflate(android.R.layout.simple_spinner_item, parent, false) : convertView;
                            TextView name = view.findViewById(android.R.id.text1);
                            name.setText(getItem(position).getName());
                            view.setPadding(30, 30, 0, 30);
                            return view;
                        }
                    };
                    area.setAdapter(areaAdapter);
                    area.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            selectedArea = areaList.get(position);
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });
                    area.setSelection(0);
                }
            }

            @Override
            public void onFailure(JSONObject data, int statusCode) {

            }

            @Override
            public void onException(Exception e) {
                e.printStackTrace();
            }
        });
        donerArrayList = new ArrayList<>();
        donationsAdapter = new DonerAdapter(donerArrayList, this);
        donerRecyclerView.setAdapter(donationsAdapter);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}